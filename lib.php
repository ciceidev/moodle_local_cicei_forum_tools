<?php


/**
 * Adds specific settings to the settings block
 *
 * @param settings_navigation $nav Current settings navigation object
 * @param context $context Current context
 */
function local_cicei_forum_tools_extends_settings_navigation(settings_navigation $nav, context $context) {
    global $DB;

    // If user can use move forums script add links
    if (has_capability('local/cicei_forum_tools_move_forums:use', $context)) {
        if ($context->contextlevel == CONTEXT_COURSE) {
            // Check if course has forums
            $modinfo = get_fast_modinfo($context->instanceid);
            if (isset($modinfo->instances['forum'])) {
                $params = array(
                    'searchcontext' => 'course',
                    'id' => $context->instanceid
                );
                $url = new moodle_url('/local/cicei_forum_tools/move_forums.php', $params);
                $icon = new pix_icon('script_go', '', 'local_cicei_forum_tools');
                // select node to add link
                $parentnode = $context->instanceid == 1 ? 'frontpage' : 'courseadmin';
                $nav->get($parentnode)->add(get_string('move_forums', 'local_cicei_forum_tools'), $url, navigation_node::TYPE_SETTING, null, 'forumsmove', $icon);
            }
        } else if ($context->contextlevel == CONTEXT_MODULE) {
            // Check if this is a forum coursemodule
            $cm = get_coursemodule_from_id('forum', $context->instanceid);
            if ($cm) {
                $forum = $DB->get_record('forum', array('id' => $cm->instance), 'id, type');
                if ($forum && $forum->type != 'single') {
                    $params = array(
                        'searchcontext' => 'forum',
                        'id' => $cm->instance,
                    );
                    $url = new moodle_url('/local/cicei_forum_tools/move_forums.php', $params);
                    $icon = new pix_icon('script_go', '', 'local_cicei_forum_tools');
                    // 0 is the forum admin node
                    $nav->get(0)->add(get_string('move_forums', 'local_cicei_forum_tools'), $url, navigation_node::TYPE_SETTING, null, 'forumsmove', $icon);
                }
            }
        }
    }
}
