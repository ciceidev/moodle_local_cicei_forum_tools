<?php

define('cicei_forum_tools_FORUM_MOVE_MARK_NO', 0);
define('cicei_forum_tools_FORUM_MOVE_MARK_SUBJECT', 1);
define('cicei_forum_tools_FORUM_MOVE_MARK_TEXT', 2);

/**
 * Execute move_forums function based in provided context and form data
 * @param type $search_context
 * @param type $course
 * @param type $form_data
 */
function cicei_forum_tools_move_forums_process_form($search_context, $course, $form_data) {
    switch ($search_context) {
        case 'course':
            cicei_forum_tools_move_forums_forums($form_data->forum_src_id, $form_data->forum_dst_id, $form_data->mark_discussions);
            break;
        case 'forum':
            cicei_forum_tools_move_forums_discussions($form_data->discussion_id, $form_data->forum_dst_id, $form_data->mark_discussions);
            break;
    }
}

/**
 * Move all discussions from a set of forums to other forums
 * @param type $forum_src_ids
 * @param type $forum_dst_ids
 */
function cicei_forum_tools_move_forums_forums($forum_src_ids, $forum_dst_ids, $mark_discussions) {
    global $DB;

    foreach ($forum_src_ids as $idx => $forum_src_id) {
        if (!isset($forum_dst_ids[$idx]) || $forum_dst_ids[$idx] == 0 || $forum_src_id == $forum_dst_ids[$idx]) {
            continue;
        }

        $forum_src = $DB->get_record('forum', array('id' => $forum_src_id));
        $forum_dst = $DB->get_record('forum', array('id' => $forum_dst_ids[$idx]));
        $discussions = $DB->get_records('forum_discussions', array('forum' => $forum_src_id));
        $moved = 0;
        foreach ($discussions as $discussion) {
            if (cicei_forum_tools_move_forums_discussion($discussion, $forum_dst, $mark_discussions)) {
                $moved++;
            }
        }

        $a = new stdClass();
        $a->moved = $moved;
        $a->count = count($discussions);
        $a->source_forum = html_writer::link(new moodle_url('/mod/forum/view.php', array('f' => $forum_src->id)), $forum_src->name);
        $a->destination_forum = html_writer::link(new moodle_url('/mod/forum/view.php', array('f' => $forum_dst->id)), $forum_dst->name);
        echo html_writer::tag('p', get_string('move_forums_forums_result', 'local_cicei_forum_tools', $a));
    }
}

/**
 * Move a set of discussions to other forums
 * @param type $discussion_ids
 * @param type $forum_ids
 */
function cicei_forum_tools_move_forums_discussions($discussion_ids, $forum_ids, $mark_discussions) {
    global $DB;

    foreach ($discussion_ids as $idx => $discussion_id) {
        if (!isset($forum_ids[$idx]) || $forum_ids[$idx] == 0) {
            continue;
        }

        $discussion = $DB->get_record('forum_discussions', array('id' => $discussion_id));
        $forumto = $DB->get_record('forum', array('id' => $forum_ids[$idx]));
        $a = new stdClass();
        $a->discussion = html_writer::link(new moodle_url('/mod/forum/discuss.php', array('d' => $discussion->id)), $discussion->name);
        $a->forum = html_writer::link(new moodle_url('/mod/forum/view.php', array('f' => $forumto->id)), $forumto->name);
        if (cicei_forum_tools_move_forums_discussion($discussion, $forumto, $mark_discussions)) {
            echo html_writer::tag('p', get_string('move_forums_discussions_result', 'local_cicei_forum_tools', $a));
        } else {
            echo html_writer::tag('p', get_string('move_forums_discussions_error', 'local_cicei_forum_tools', $a));
        }
    }
}

/**
 * Move a discussion to another forum. Based in mod/forum move discussion code.
 * @global type $CFG
 * @global type $DB
 * @param type $discussion
 * @param type $forumto
 * @param type $mark_discussions
 * @return boolean
 */
function cicei_forum_tools_move_forums_discussion($discussion, $forumto, $mark_discussions) {
    global $CFG, $DB;

    // Invalid inputs
    if (empty($discussion) || empty($forumto)) {
        return false;
    }

    // Move attachments
    require_once($CFG->dirroot.'/mod/forum/lib.php');
    forum_move_attachments($discussion, $discussion->forum, $forumto->id);

    // Get source forum
    $forumsrc = $DB->get_record('forum', array('id' => $discussion->forum));

    // Mark discussions as needed
    switch ($mark_discussions) {
        case cicei_forum_tools_FORUM_MOVE_MARK_SUBJECT:
            $append_text = get_string('move_forums_mark_text_subject', 'local_cicei_forum_tools', $forumsrc->name);
            $DB->set_field('forum_discussions', 'name', $discussion->name . ' ' . $append_text, array('id' => $discussion->id));
            $DB->set_field('forum_posts', 'subject', $discussion->name . ' ' . $append_text, array('discussion' => $discussion->id, 'parent' => 0));
            break;
        case cicei_forum_tools_FORUM_MOVE_MARK_TEXT:
            $post = $DB->get_record('forum_posts', array('discussion' => $discussion->id, 'parent' => 0));
            if ($post->messageformat == FORMAT_HTML) {
                $append_text = "<hr>". get_string('move_forums_mark_text_append', 'local_cicei_forum_tools', $forumsrc->name);
            } else {
                $append_text = "\n". get_string('move_forums_mark_text_append', 'local_cicei_forum_tools', $forumsrc->name);
            }
            $DB->set_field('forum_posts', 'message', $post->message . $append_text, array('discussion' => $discussion->id, 'parent' => 0));
            break;
        case cicei_forum_tools_FORUM_MOVE_MARK_NO:
        default:
            break;
    }

    // Update tables
    $DB->set_field('forum_discussions', 'forum', $forumto->id, array('id' => $discussion->id));
    $DB->set_field('forum_read', 'forumid', $forumto->id, array('discussionid' => $discussion->id));
    // NOTE: mod/forum don't do anything with ratings, but this could be also be moved, but take into account that
    // destination forum could have a different ratings system
    //$DB->set_field('rating', 'contextid', $newcontext->id, array('contextid' => $oldcontext->id));

    // Delete the RSS files for the 2 forums to force regeneration of the feeds
    require_once($CFG->libdir . '/rsslib.php');
    require_once($CFG->dirroot . '/mod/forum/rsslib.php');
    forum_rss_delete_file($forumsrc);
    forum_rss_delete_file($forumto);

    return true;
}