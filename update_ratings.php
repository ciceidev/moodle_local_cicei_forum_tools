<?php

// Allow execution though cli and web access
if (php_sapi_name() == 'cli') {
    define('CLI_SCRIPT', true);
    require('../../config.php');
} else {
    require('../../config.php');
    require_login();
    require_capability('moodle/site:config', context_system::instance());
}

global $DB;

// Determine if update is needed
$count = $DB->count_records('rating', array('component' => ''));
if ($count == 0) {
    die("No ratings to update were found\n");
}

echo "Starting ratings update\n";

$ratings = $DB->get_records_sql("SELECT DISTINCT contextid FROM {rating} WHERE component=''");
foreach ($ratings as $rating) {
    $contextid = $rating->contextid;
    $context = context::instance_by_id($contextid);
    $cm = get_coursemodule_from_id(false, $context->instanceid);
    if ($cm->modname == 'forum') {
        $component = 'mod_forum';
        $ratingarea = 'post';
    } else if ($cm->modname == 'glossary') {
        $component = 'mod_glossary';
        $ratingarea = 'entry';
    } else {
        echo "Rating with context $contextid and modulename $cm->modname was unable to be updated\n";
        continue;
    }
    $DB->execute("UPDATE {rating} SET component='$component', ratingarea='$ratingarea' WHERE contextid=$contextid");
}

echo "Ratings update ended\n";