<?php

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}

require_once($CFG->libdir . '/formslib.php');

class local_cicei_forum_tools_move_forums_form extends moodleform {

    function definition() {
        global $DB;

        $mform = & $this->_form;

        $course = $this->_customdata['course'];
        $current_forum = $this->_customdata['forum'];
        $searchcontext = $this->_customdata['searchcontext'];

        // Grab course forums
        $forums = array(0 => '');
        foreach ($DB->get_records('forum', array('course' => $course->id), '', 'id, name, type') as $forum) {
            if ($forum->type != 'single') {
                $forums[$forum->id] = $forum->name;
            }
        }

        if ($searchcontext == 'course') {
            // In course context, put multiple source/destination select forum elements
            // Put a header
            $mform->addElement('header', 'general', get_string('move_forums_section', 'local_cicei_forum_tools'));
            $mform->addHelpButton('general', 'move_forums_section', 'local_cicei_forum_tools');

            // Group selection
            $select_group = array();
            $forums[0] = get_string('select_src_forum', 'local_cicei_forum_tools');
            $select_group[] = $mform->createElement('select', 'forum_src_id', '', $forums);
            $forums[0] = get_string('select_dst_forum', 'local_cicei_forum_tools');
            $select_group[] = $mform->createElement('select', 'forum_dst_id', '', $forums);
            $select_group = $mform->createElement('group', 'forum_src_dst_selection', get_string('move_forums_group', 'local_cicei_forum_tools'), $select_group, '&nbsp;<strong style="font-size: 20px;">&rarr;</strong>&nbsp;', false);

            // Allow repeations
            $this->repeat_elements(array($select_group), 1, array(), 'option_repeats', 'option_add_fields', 3);
        } elseif ($searchcontext == 'forum') {
            // In forum context, put a destination select forum for each discussion in the forum
            // Put a header
            $mform->addElement('header', 'general', get_string('discussions_move_section', 'local_cicei_forum_tools'));
            $mform->addHelpButton('general', 'discussions_move_section', 'local_cicei_forum_tools');

            // Default forum selection
            $forums[0] = get_string('select_dst_forum', 'local_cicei_forum_tools');

            // Grab discussion from current forum
            $discussions = array();
            $conditions = array(
                'course' => $course->id,
                'forum' => $current_forum->id
            );
            $count = 0;
            foreach ($DB->get_records('forum_discussions', $conditions) as $discussion) {
                $select_group = array();
                $select_group[] = $mform->createElement('hidden', "discussion_id[$count]", $discussion->id);
                $select_group[0]->setType('id', PARAM_INT);
                $select_group[] = $mform->createElement('static', 'discussion_name', '', $discussion->name, array('style' => 'width: 300px;'));
                $select_group[] = $mform->createElement('select', "forum_dst_id[$count]", '', $forums);
                $mform->addGroup($select_group, 'discussion_dst_forum_selection', '', '&nbsp;<strong style="font-size: 20px;">&rarr;</strong>&nbsp;', false);
                $count++;
            }
        }

        // Select mark in moved discussions
        $mform->addElement('header', 'general', get_string('move_forums_mark_section', 'local_cicei_forum_tools'));
        $select_mark = array(
            cicei_forum_tools_FORUM_MOVE_MARK_NO => get_string('move_forums_mark_no', 'local_cicei_forum_tools'),
            cicei_forum_tools_FORUM_MOVE_MARK_SUBJECT => get_string('move_forums_mark_subject', 'local_cicei_forum_tools'),
            cicei_forum_tools_FORUM_MOVE_MARK_TEXT => get_string('move_forums_mark_text', 'local_cicei_forum_tools'),
        );
        $mform->addElement('select', 'mark_discussions', get_string('move_forums_mark', 'local_cicei_forum_tools'), $select_mark);

        // Put warning info
        $mform->addElement('html', html_writer::tag('h4', get_string('discussions_move_warning', 'local_cicei_forum_tools'), array('class' => 'text-error')));

        // Add submit button
        $this->add_action_buttons(false, get_string('submit_button', 'local_cicei_forum_tools'));
    }
}

?>
