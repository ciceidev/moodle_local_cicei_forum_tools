<?php

$capabilities = array(
    // Capability to use move forums tool
    'local/cicei_forum_tools_move_forums:use' => array(
        'riskbitmask' => RISK_CONFIG,
        'captype' => 'write',
        'contextlevel' => CONTEXT_COURSE,
        'archetypes' => array(
            'manager' => CAP_ALLOW
        )
    ),
);

