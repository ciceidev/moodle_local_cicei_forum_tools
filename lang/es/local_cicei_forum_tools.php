<?php

// Plugin name
$string['pluginname'] = 'CICEI - Herramientas de foros';

// Move forums texts
$string['discussions_move_section'] = 'Selección de discusiones a mover';
$string['discussions_move_section_help'] = 'Aquí puedes seleccionar un foro destino para cada discusión para moverlas a ese foro.';
$string['discussions_move_warning'] = 'ATENCIÓN: se perderán todas las calificaciones al mover discusiones entre foros.';
$string['move_forums_forums_result'] = 'Se movieron {$a->moved} de {$a->count} discusiones de <strong>{$a->source_forum}</strong> a <strong>{$a->destination_forum}</strong>.';
$string['move_forums_discussions_result'] = 'Se movió <strong>{$a->discussion}</strong> a <strong>{$a->forum}</strong>.';
$string['move_forums_discussions_error'] = 'No se pudo mover {$a->discussion} a {$a->forum}.';
$string['move_forums_mark'] = 'Selecciona como marcar las discusiones movidas';
$string['move_forums_mark_no'] = 'No marcar las discusiones';
$string['move_forums_mark_section'] = 'Marcar las discusiones movidas';
$string['move_forums_mark_subject'] = 'Poner una marca al final de asunto';
$string['move_forums_mark_text'] = 'Poner una marca al final del mensaje';
$string['move_forums_mark_text_append'] = 'Movido desde {$a}.';
$string['move_forums_mark_text_subject'] = '(Movido desde {$a})';
$string['move_forums_section'] = 'Selección de foros a mover';
$string['move_forums_section_help'] = 'Aquí puedes seleccionar un foro de origen y otro de destino para mover todas las discusiones de uno al otro.';
$string['move_forums'] = 'Mover foros y discusiones';
$string['move_forums_group'] = 'Selecciona el foro origen y destino.';
$string['page_title'] = 'Mover discusiones entre foros';
$string['select_src_forum'] = 'Foro origen';
$string['select_dst_forum'] = 'Foro destino';
$string['submit_button'] = 'Procesar';