<?php

// Plugin name
$string['pluginname'] = 'CICEI - Forum Tools';

// Move forums texts
$string['discussions_move_section'] = 'Discussions movement selection';
$string['discussions_move_section_help'] = 'Here you can select a destination forums for each discussion, so all this discussions will be moved to selected forums.';
$string['discussions_move_warning'] = 'WARNING: all ratings will be lost when moving discussions between forums.';
$string['move_forums_forums_result'] = 'Moved {$a->moved} of {$a->count} discussions from <strong>{$a->source_forum}</strong> to <strong>{$a->destination_forum}</strong>.';
$string['move_forums_discussions_result'] = 'Moved <strong>{$a->discussion}</strong> to <strong>{$a->forum}</strong>.';
$string['move_forums_discussions_error'] = 'Cannot move {$a->discussion} to {$a->forum}.';
$string['move_forums_mark'] = 'Select how to mark moved discussions';
$string['move_forums_mark_no'] = 'Don\'t mark moved discussions';
$string['move_forums_mark_section'] = 'Mark moved discussions';
$string['move_forums_mark_subject'] = 'Put a mark at the end of the subject';
$string['move_forums_mark_text'] = 'Put a mark at the end of the message';
$string['move_forums_mark_text_append'] = 'Moved from {$a}.';
$string['move_forums_mark_text_subject'] = '(Moved from {$a})';
$string['move_forums_section'] = 'Move forums selection';
$string['move_forums_section_help'] = 'Here you can select source and destination forums in pairs, so all discussions in source forum will be moved to destination forum.';
$string['move_forums'] = 'Move forums and discussions';
$string['move_forums_group'] = 'Select source and destination forum';
$string['page_title'] = 'Move discussions between forums';
$string['select_src_forum'] = 'Source forum';
$string['select_dst_forum'] = 'Destination forum';
$string['submit_button'] = 'Process';