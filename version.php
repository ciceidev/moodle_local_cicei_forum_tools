<?php

$plugin->version  = 2013082500;
$plugin->requires = 2011120500;  // Requires this Moodle version
$plugin->cron     = 0;           // Period for cron to check this module (secs)

$plugin->component = 'local_cicei_forum_tools';
$plugin->maturity  = MATURITY_STABLE;

$plugin->release = '1.0';             // User-friendly version number
