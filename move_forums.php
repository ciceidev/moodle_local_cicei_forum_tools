<?php

require_once('../../config.php');

global $PAGE, $DB, $OUTPUT, $CFG;

// Input params
$searchcontext = required_param('searchcontext', PARAM_ALPHANUMEXT);
$id = required_param('id', PARAM_INT);

// Build url
$params = array(
    'searchcontext' => $searchcontext,
    'id' => $id,
);
$page_url= new moodle_url('/local/cicei_forum_tools/move_forums.php', $params);
$PAGE->set_url($page_url);

// Initialize vars
$context = NULL;
$cm = NULL;
$course = NULL;
$forum = NULL;

// Get course and context
switch ($searchcontext) {
    case 'course':
        $context = context_course::instance($id);
        $course = $DB->get_record('course', array('id' => $id), '*', MUST_EXIST);
        $forum = NULL;
        $cm = NULL;
        $title = get_string('page_title', 'local_cicei_forum_tools', $course->fullname);
        break;
    case 'forum':
        $cm = get_coursemodule_from_instance('forum', $id);
        $context = context_module::instance($cm->id);
        $course = $DB->get_record('course', array('id' => $cm->course), '*', MUST_EXIST);
        $forum = $DB->get_record('forum', array('id' => $id), '*', MUST_EXIST);
        $title = get_string('page_title', 'local_cicei_forum_tools', $course->fullname);
        break;
    default:
        notice(get_string('error', 'moodle'));
}

// Check login
require_course_login($course, true, $cm);

// Configure page
$PAGE->set_context($context);
$PAGE->set_title($title);
$PAGE->set_heading(format_string($course->fullname));
$PAGE->set_pagelayout('base');
$PAGE->navbar->add($title);

// Begin page
echo $OUTPUT->header();
echo $OUTPUT->heading($title);

// Check if user has capability to use the plugin
require_capability('local/cicei_forum_tools_move_forums:use', $context);

// Move forums lib
require_once('move_forums_lib.php');
// Form to config analysis
require_once('move_forums_form.php');

// Create form
$custom_data = array(
    'course' => $course,
    'forum' => $forum,
    'searchcontext' => $searchcontext,
);
$mform_post = new local_cicei_forum_tools_move_forums_form($page_url->out(false), $custom_data);

// Try to get data form form
if ($fromform = $mform_post->get_data()) {
    cicei_forum_tools_move_forums_process_form($searchcontext, $course, $fromform);
} else {
    // show form
    $mform_post->display();
}

// End page
echo $OUTPUT->footer($course);